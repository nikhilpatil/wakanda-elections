import React from "react";

class ReadString extends React.Component {

  state = { dataKey: null };

  componentDidMount() {
    const { drizzle, drizzleState } = this.props;
    const contract = drizzle.contracts.WakandaElections;
    
    // let drizzle know we want to watch the `myString` method
    const dataKey = contract.methods["candidates"].cacheCall(1);

    // save the `dataKey` to local component state for later reference
    this.setState({ dataKey });

    // const dataKey = contract.methods["candidates"].cacheCall(1, {
    //     from: drizzleState.accounts[0]
    // });
    //const myString = contract.candidates.cacheCall();
    // let drizzle know we want to call the `set` method with `value`
    // const stackId = contract.methods["vote"].cacheSend(1, {
    //     from: drizzleState.accounts[0]
    // });
    
    //console.log(drizzleState);
   // console.log(contract.methods.candidates(1).call());
  }

  render() {
      // get the contract state from drizzleState
      const { WakandaElections } = this.props.drizzleState.contracts;
      console.log(WakandaElections);

      // using the saved `dataKey`, get the variable we're interested in
      const myString = WakandaElections.candidates[this.state.dataKey];
      if(myString){
        console.log(myString.value.name);
      }

      // if it exists, then we display its value
      return <p>My stored string: {}</p>;
  }
}

export default ReadString;