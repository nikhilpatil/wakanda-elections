import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ReadString from './ReadString';
import ListCandidates from './ListCandidates';
import CandidatesVotes from './CandidatesVotes';

class App extends Component {

  state = { loading: true, drizzleState: null, selectedCanditate: '' };

  componentDidMount() {
    const { drizzle } = this.props;

    // subscribe to changes in the store
    this.unsubscribe = drizzle.store.subscribe(() => {

      // every time the store updates, grab the state from drizzle
      const drizzleState = drizzle.store.getState();

      // check to see if it's ready, if so, update local component state
      if (drizzleState.drizzleStatus.initialized) {
        this.setState({ loading: false, drizzleState });
      }
    });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }



  render() {
    if (this.state.loading) return "Loading Drizzle...";
    return (<div>
      <div className="m-3">
        <h1 className="display-5">Wakanda Presedential Election (2019)</h1>
        <p className="lead">Let's vote for the next president of Wakanda</p>
        <hr className="my-4"></hr>        
        <CandidatesVotes drizzle={this.props.drizzle} drizzleState={this.state.drizzleState} />        
      </div>
      <div>
        <ListCandidates drizzle={this.props.drizzle} drizzleState={this.state.drizzleState} />
      </div>
    </div>);
  }
}

export default App;
