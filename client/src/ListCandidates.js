import React from 'react';
import './ListCandidates.css';

function ShowCandidates(props){
    return(<div className="profile">
    <div>
    <select required
        className="custom-select"                        
        id="selectedOption"
        onChange={props.onChange}
    >
        {props.candidates}
    </select>
</div>
<div>
{props.state.selectedCandiateName}
</div>
<div>
{props.state.selectedCandiateDesc}
</div>
<div>
<button type="button" className="btn btn-success btn-lg" onClick={props.onClick}>Vote</button> 
</div>
</div>);
}

export default class ListCandidates extends React.Component {

    constructor(props){
        super(props);
        this.candidateKeys = [];
        this.state = { 
            candidates: [
                'Candidate 1', 'Candidate 2', 'Candidate 3', 'Candidate 4'
            ],
            candidateKeys: [],
            selectedCandiateId: 0,
            selectedCandiateName: '',
            selectedCandiateDesc: '',
            currentAccount: 0,
            hasIVoted: false,
            votingDone: false,
            voterKey:''       
        };
    }

    componentWillMount(){

    const { drizzle, drizzleState } = this.props;
    
    if(drizzleState.drizzleStatus.initialized){
        const firstCandidateKey = drizzle.contracts.WakandaElections.methods.candidates.cacheCall(1);
        const secondCandidateKey = drizzle.contracts.WakandaElections.methods.candidates.cacheCall(2);
        const thirdCandidateKey = drizzle.contracts.WakandaElections.methods.candidates.cacheCall(3);
        const fourthCandidateKey = drizzle.contracts.WakandaElections.methods.candidates.cacheCall(4);

        this.setState({ firstCandidateKey, secondCandidateKey, thirdCandidateKey, fourthCandidateKey});                

        const firstVoterKey = drizzle.contracts.WakandaElections.methods.voters.cacheCall(drizzleState.accounts[this.state.currentAccount]);        
        this.setState({
            voterKey: firstVoterKey
        });        
    }    
    }

    setVote = value => {
        const { drizzle, drizzleState } = this.props;
        const contract = drizzle.contracts.WakandaElections;
    
        // let drizzle know we want to call the `set` method with `value`
        const stackId = contract.methods["vote"].cacheSend(value, {
          from: drizzleState.accounts[this.state.currentAccount]
        });
    
        // save the `stackId` for later reference
        this.setState({ stackId, votingDone: true });
    };

    onChange = (event) => {
        const { WakandaElections } = this.props.drizzleState.contracts;

        let id = String(event.target.value);
        
        let dataKey = '';
        if(id === "Candidate 1"){
            dataKey = this.state.firstCandidateKey;
        }
        else if(id === "Candidate 2"){
            dataKey = this.state.secondCandidateKey;
        }
        else if(id === "Candidate 3"){
            dataKey = this.state.thirdCandidateKey;
        }
        else if(id === "Candidate 4"){
            dataKey = this.state.fourthCandidateKey;
        }
        const data = WakandaElections.candidates[dataKey].value;
        if(data){
            this.setState({
                selectedCandiateName: data.name,
                selectedCandiateDesc: data.desc
            });
        }

        let voteId = parseInt(id.split(" ")[1]);
        this.setState({
            selectedCandiateId: voteId
        });
    }

    vote = () => {
        if(this.state.selectedCandiateId !== 0){
            this.setVote(this.state.selectedCandiateId);
        }
    }

    hasIAlreadyVoted = () => {
        const { WakandaElections } = this.props.drizzleState.contracts;        
        const myString = WakandaElections.voters[this.state.voterKey];
        if(myString){
            return myString.value;
        }
        return false;
    }
    
    getTxStatus = () => {
        // get the transaction states from the drizzle state
        const { transactions, transactionStack } = this.props.drizzleState;
    
        // get the transaction hash using our saved `stackId`
        const txHash = transactionStack[this.state.stackId];
    
        // if transaction hash does not exist, don't display anything
        if (!txHash) return null;
    
        // otherwise, return the transaction status
        return `Transaction status: ${transactions[txHash].status}`;
    };

    render(){
        this.props.drizzle.contracts.WakandaElections.events
        .votedEvent({/* eventOptions */}, (error, event) => {
            if(event){
                this.setState({
                    hasIVoted: true
                });
            }
        });
        // .on('data', (event) => {console.log(event)})
        // .on('changed', (event) => console.log(event))
        // .on('error', (error) => console.log(error));
           
        let candidates = this.state.candidates;
        let listCandidates = [];
        listCandidates[0] = (
            <option key={0} value="" hidden>
                Select your option
            </option>
        );

        for (let i = 0; i < candidates.length; i++) {
            listCandidates[i + 1] = (
                <option key={i+1} value={candidates[i]}>{candidates[i]}</option>
            );
        }

        return(<div>         

        {

            this.state.votingDone ? (<div className="m-5"><h3>{this.getTxStatus()}</h3></div>) : (
                this.hasIAlreadyVoted() ? (<div className="m-5"><h3>You have already voted</h3></div>) : (<ShowCandidates 
                    state={this.state} 
                    candidates={listCandidates} 
                    onChange={this.onChange} 
                    onClick={this.vote}
                    />)
            )
        }


        </div>);        
    }
}