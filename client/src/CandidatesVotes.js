import React from 'react';

export default class CandidatesVotes extends React.Component {

    constructor(props){
        super(props);
        this.stat = {
            WakandaElections: ''
        }
    }

    componentWillMount(){

    const { drizzle, drizzleState } = this.props;
    this.setState({
        WakandaElections: drizzleState.contracts
    });
        
    if(drizzleState.drizzleStatus.initialized){
        const firstCandidateKey = drizzle.contracts.WakandaElections.methods.candidates.cacheCall(1);
        const secondCandidateKey = drizzle.contracts.WakandaElections.methods.candidates.cacheCall(2);
        const thirdCandidateKey = drizzle.contracts.WakandaElections.methods.candidates.cacheCall(3);
        const fourthCandidateKey = drizzle.contracts.WakandaElections.methods.candidates.cacheCall(4);

        this.setState({ firstCandidateKey, secondCandidateKey, thirdCandidateKey, fourthCandidateKey});                

        }    
    }

    getData = (id) => {
        const { WakandaElections } = this.props.drizzleState.contracts;
                
        let dataKey = '';
        if(id === "Candidate 1"){
            dataKey = this.state.firstCandidateKey;
        }
        else if(id === "Candidate 2"){
            dataKey = this.state.secondCandidateKey;
        }
        else if(id === "Candidate 3"){
            dataKey = this.state.thirdCandidateKey;
        }
        else if(id === "Candidate 4"){
            dataKey = this.state.fourthCandidateKey;
        }
        const data = WakandaElections.candidates[dataKey];
        if(data){
           return [data.value.name, data.value.voteCount];
        }

        return "-";
    }

    render(){
        let candidate1 = this.getData("Candidate 1");
        let candidate2 = this.getData("Candidate 2");
        let candidate3 = this.getData("Candidate 3");
        let candidate4 = this.getData("Candidate 4");

        return(<div>
            <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Candidate</th>
      <th scope="col">Vote</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>          
      <td>{candidate1[0]}</td>
      <td>{candidate1[1]}</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>{candidate2[0]}</td>
      <td>{candidate2[1]}</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>{candidate3[0]}</td>
      <td>{candidate3[1]}</td>
    </tr>
    <tr>
      <th scope="row">4</th>
      <td>{candidate4[0]}</td>
      <td>{candidate4[1]}</td>
    </tr>
  </tbody>
</table>
        </div>);
    }
}